# convert from mkv to mp4
ffmpeg -i "windrunnerfortnite-intro-1080p-60fps.mkv" -c:v h264_nvenc -b:v 10M "output2.mp4"

# trim video #1
ffmpeg -ss 00:00:05 -i input.mp4 -c copy output.mp4
ffmpeg -i input.mp4 -ss 00:00:05 -c:v copy -c:a copy output.mp4

# trim video #2
ffmpeg -i input.mp4 -ss 01:19:27 -to 02:18:51 -c:v copy -c:a copy output.mp4

# trim video #3
ffmpeg -i input.mp4 -ss 00:01:10 -t 00:01:05 -c:v copy -c:a copy output.mp4

# create short video from an image and mp3 file
ffmpeg.exe -r 1/5 -i ".\dups\1080p2.jpg" -i "5secondsong.mp3" -c:a copy -shortest -c:v libx264 -vf "fps=30,format=yuv420p" transition.mp4

# regulate video to 1080p 8mbps
ffmpeg -i "Fortnite 05-27-2020 0-42-39-122a.mp4" -c:v h264_nvenc -vf "scale=1920:1080,fps=30,format=yuv420p" -b:v 8M output.mp4

# create slowmo video, also setting fps
ffmpeg.exe -i .\c3-headshot-44100.mp4 -filter:v "setpts=4*PTS,fps=30" -filter:a "atempo=0.5,atempo=0.5" -c:v h264_nvenc .\c3-headshot-44100-h264-0.25x.mp4

# set sampling rate for audio
ffmpeg.exe -i .\c1-diving.mp4 -ar 44100 .\c1-diving-44100.mp4

# shift audio by -1 second
ffmpeg.exe -itsoffset 1 -i ".\Sounds.mp4" -i ".\Sounds.mp4" -map 0:v -map 1:a -ss 08:50 -t 01:21 -c copy -shortest output2.mp4

# extract audio from mp4
ffmpeg.exe -i ".\Sounds.mp4" -ss 00:10:10 -c:a mp3 -b:a 160K "Fortnite 06-11-2020 17-47-28-193-v2.mp3"

# sound volume
ffmpeg.exe -i "input.mp4" -filter:a "volume=2" "output.mp4"

# filter all video with 30 fps and normal volume
dir *.mp4 | % {ffmpeg.exe -i "$($_.name)" -c:v h264_nvenc -filter:v "fps=30" -filter:a "loudnorm" -b:v 8M -b:a 160K -ar 44100 "$($_.name.replace(".mp4","-filtered.mp4"))"}

# Simple Overlay with Colorkey example
ffmpeg -i .\clip1.mp4 -i .\clip2.mp4 -filter_complex '[1:v]colorkey=0xff0000:0.3:0.2[ckout];[0:v][ckout]overlay[out]' -map '[out]' output.mp4

# Simple Overlay starting 35s 
ffmpeg -i .\clip1-freeze-43s.mp4 -i .\clip2.mp4 -filter_complex "[0:v]setpts=PTS-STARTPTS[v0];[1:v]setpts=PTS-STARTPTS+35/TB[v1];[v0][v1]overlay=eof_action=pass[out1]" -map [out1] -c:v h264_nvenc -b:v 8M -b:a 160K final.mp4

# Add water mark
ffmpeg -i '.\Fortnite_09-07-2022_17-38-14-93-clip intro 20-23 freeze 22.5-clipped-20-03.mp4' -i .\red-circle-120px.png -filter_complex "overlay=W-270:H-490:format=auto:enable='between(t,2,5)',format=yuv420p" -c:a copy output2.mp4



*** Outro Steps ***

# add 7s freeze frame starting 35s, for outro clip
ffmpeg -i .\clip1.mp4 -vf "setpts='PTS-STARTPTS + gte(T,35)*(7/TB)'" -af "asetpts='PTS-STARTPTS + gte(T,35)*(7/TB)',aresample=async=1:first_pts=0" -vsync cfr -c:v h264_nvenc -b:v 8M -b:a 160K -ar 44100 clip1-freeze-42.13s.mp4

# Overlay clip2 on clip1 with colorkey Red at position 35s
ffmpeg -i .\clip1-freeze-43s.mp4 -i .\clip2.mp4 -filter_complex "[0:v]setpts=PTS-STARTPTS[v0];[1:v]colorkey=0xff0000:0.3:0.2,setpts=PTS-STARTPTS+35/TB[v1];[v0][v1]overlay=eof_action=pass[out1]" -map [out1] -c:v h264_nvenc -b:v 8M -b:a 160K final-colorkey.mp4

# combine videos for full sound track
ffmpeg -i .\clip1.mp4 -ss 00:00 -t 35 -i .\clip2-7s.mp4 -filter_complex "[0:v] [0:a] [1:v] [1:a] concat=n=2:v=1:a=1 [v] [a]" -map "[v]" -map "[a]" -c:v h264_nvenc -b:v 8M -b:a 160K -ar 44100 totalsound.mp4

# apply sound track to video
ffmpeg -i '.\overlayed-with colorkey-at 35s.mp4' -i .\totalsound.mp4 -map 0:v -map 1:a -c:v h264_nvenc -b:v 8M -b:a 160K -ar 44100 "overlayed-with colorkey-at 35s-with sound clip1.mp4"

*** Outro Steps ***


# *** Keep (right trim) script
dir "m02c02-Fortnite 06-16-2020 1-07-02-409-keep 18s.mp4" | % {ffmpeg.exe -i "$($_.name)" -t 18 -c:v h264_nvenc -b:v 8M -b:a 160K -ar 44100 "$($_.name.replace(".mp4","-kept.mp4"))"}


# convert 90 degrees
 dir * -include *.mp4,*.mov | % {ffmpeg.exe -i ".\$filtering\$($_.name)" -c:v h264_nvenc -filter:v "scale=607:1080,fps=30,format=yuv420p" -filter:a "loudnorm" -b:v 8M -b:a 160K -ar 44100 ".\filtered-$($_.name)"}
 dir * -Include *.mp4,*.mov,*.m4v | % {ffmpeg -i $_.name -filter_complex "[0:v]scale=ih*16/9:-1,boxblur=luma_radius=min(h\,w)/20:luma_power=1:chroma_radius=min(cw\,ch)/20:chroma_power=1[bg];[bg][0:v]overlay=(W-w)/2:(H-h)/2,crop=h=iw*9/16" -c:v h264_nvenc -b:v 8M "converted-$($_.name)"}
 dir conver* | % {rename-item $_.name $_.name.replace(".mp4","-converted-vertical.mp4").replace(".MOV","-converted-vertical.MOV").replace("converted-filtered-","")}